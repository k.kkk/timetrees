﻿using System;
using System.IO;
using System.Globalization;
using System.Linq;
using System.Text;
using Newtonsoft.Json;
using System.Collections.Generic;
using Microsoft.VisualBasic;
namespace TimeTrees
{
    struct TimelineEvent
    {
        public DateTime date;
        public string title;

    }
    struct Person
    {
        public int id;
        public string name;
        public DateTime birth;
        public DateTime? death;

    }
    class Program
    {
        const int DateIndex = 0;
        const int EventIndex = 1;
        const int PersonIndex = 0;
        const int NameIndex = 1;
        const int BirthIndex = 2;
        const int DeathIndex = 3;

        static void Main(string[] args)
        {
            Console.WriteLine("Какой формат файла вам нужен?");
            var format = Console.ReadLine();


            string path = $"..\\..\\..\\timeline.{format}";
            string path2 = $"..\\..\\..\\people.{format}";

            TimelineEvent[] TimeLine = FormatFileTimeline(format, path);
            Person[] People = FormatFilePeople(format, path2);
            (int years, int months, int days) = ResultTimeLine(TimeLine);
            var name = Name(People);
            Console.WriteLine($"Между максимальной и минимальной датами прошло: {years} лет, {months} месяцев и {days} дней");

            if (OutputOrNot(People) == true)
            {
                Console.WriteLine($"Имя человека который родился в високосный год и его возраст не более 20 лет - {name} ");
            }
            WriteTimelineJson(path, TimeLine);
            WritePersonJson(path2, People);

        }

        static TimelineEvent[] FormatFileTimeline(string format, string path)
        {
            TimelineEvent[] timeline = default;
            if (format == "csv")
            {
                 timeline = ReadFile(path);
            }
            else
            {
                 timeline = FileTimelineJson(path);
            }
            return timeline;
            

        }
        
        static Person[] FormatFilePeople(string format, string path2)
        {
            Person[] people = default;
            if (format == "csv")
            {
                people = ReadFile2(path2);
            }
            if (format == "json")
            {
                people = FilePeopleJson(path2);
            }
            return people;
        }

        static TimelineEvent[] FileTimelineJson(string path)
        {
            string json = File.ReadAllText(path);
            return JsonConvert.DeserializeObject<TimelineEvent[]>(json);
        }

        static Person[] FilePeopleJson(string path2)
        {
            string json = File.ReadAllText(path2);
            return JsonConvert.DeserializeObject<Person[]>(json);
        }

        static TimelineEvent[] ReadFile(string path)//Считываем
        {
            string[] lines = File.ReadAllLines(path);
            int len = lines.Length;
            TimelineEvent[] timeLines = new TimelineEvent[len];
            for (var i = 0; i < len; i++)
            {
                TimelineEvent parts;
                string[] splitData = lines[i].Split(';');
                parts.date = ParseDate(splitData[DateIndex]);
                parts.title = splitData[EventIndex];
                timeLines[i] = parts;

            }
            return timeLines;
        }

        static DateTime ParseDate(string lines)//Преобразовываем в DateTime
        {
            DateTime date;

            if (!DateTime.TryParseExact(lines, "yyyy", CultureInfo.InvariantCulture, DateTimeStyles.None, out date))
            {
                if (!DateTime.TryParseExact(lines, "yyyy-MM", CultureInfo.InvariantCulture, DateTimeStyles.None, out date))
                {
                    if (!DateTime.TryParseExact(lines, "yyyy-MM-dd", CultureInfo.InvariantCulture, DateTimeStyles.None, out date))
                    {

                        date = default;
                    }
                }
            }
            return date;
        }

        static (DateTime, DateTime) MinAndMax(TimelineEvent[] timeline)//Находим самую раннюю и позднюю дату
        {
            DateTime minDate = DateTime.MaxValue;
            DateTime maxDate = DateTime.MinValue;
            for (var i = 0; i < timeline.Length; i++)
            {
                var date = timeline[i].date;

                if (minDate > date)
                {
                    minDate = date;
                }
                if (maxDate < date)
                {
                    maxDate = date;
                }
            }

            return (minDate, maxDate);
        }

        static (int, int, int) ResultTimeLine(TimelineEvent[] timeline)//Находим количество лет, месяцев, дней между самой ранней и поздней датой
        {
            (var earlyDate, var lateDate) = MinAndMax(timeline);
            int changeMonths = 0;
            int changeYears = 0;

            int day = lateDate.Day - earlyDate.Day;

            if (day < 0)
            {
                day = day + DateTime.DaysInMonth(earlyDate.Year, lateDate.Month);
                changeMonths = 1;
            }

            var month = lateDate.Month - earlyDate.Month - changeMonths;

            if (month < 0)
            {
                month = month + 12;
                changeYears = 1;
            }

            var year = lateDate.Year - earlyDate.Year - changeYears;
            return (year, month, day);
        }
        static Person[] ReadFile2(string path2)//Считываем
        {
            string[] lines2 = File.ReadAllLines(path2);
            int len2 = lines2.Length;

            Person[] people = new Person[len2];

            for (var i = 0; i < people.Length; i++)
            {
                Person parts;
                string[] splitData2 = lines2[i].Split(';');
                parts.id = Convert.ToInt32(splitData2[PersonIndex]);
                parts.name = splitData2[NameIndex];
                parts.birth = ParseDate(splitData2[BirthIndex]);
                if (len2 == 4)
                {
                    parts.death = default;
                }
                else
                    parts.death = DateTime.Now;
                people[i++] = parts;
            }
            return people;

        }

        static bool OutputOrNot(Person[] people)
        {

            bool yesOrNot = default;
            
            foreach (var person in people)
            {
                var birth = person.birth;
                DateTime death = person.death?? default;
                int year1 = birth.Year;
                int year2 = death.Year;
                int delta = year2 - year1;
                int year = birth.Year;
                yesOrNot = (year % 4 == 0 && year % 100 != 0 | year % 400 == 0) && (delta < 21);
            }
            return yesOrNot;
        }
        static string Name(Person[] people)
        {
            string name = default;
            for (var i = 0; i < people.Length; i++)
            {
                name = people[i].name;

            }
            return name;
        }

        static void WriteTimelineJson(string path, TimelineEvent[] timeline)
        {
            string json = JsonConvert.SerializeObject(timeline);
            File.WriteAllText(path, json);
        }

        static void WritePersonJson(string path2, Person[] people)
        {
            string json = JsonConvert.SerializeObject(people);
            File.WriteAllText(path2, json);
        }
    }
}